import sys

class task_frame:

    def add_task(self, name, description, priority, due_date, project_name):
        self.name = name
        self.description = description
        self.priority = priority
        self.due_date = due_date
        self.project_name = project_name

        line = self.name + '|' + self.description + '|' + self.priority + '|' + self.due_date + '|' + self.project_name + '\n'

        with open('tasks', 'a') as t:
            t.write(line);

        t.close();


def create_task():
    task = task_frame();
    name = raw_input("Enter task name: ")
    description = raw_input('Enter task description: ')
    priority = raw_input('Enter task priority: ')
    due_date = raw_input('Enter task due date: ')
    project_name = raw_input('Enter project name: ')

    task.add_task(name, description, priority, due_date, project_name)

#def search(arguments):
#    parameter = arguments[2];
#    for item in parameter:
#        if item.lower


def redirect(arguments):
    flag = 0
    with open('tasks', 'r') as t:
        tasks = t.readlines()
    for line in tasks:
        entry = line.split('|')
        taskName = entry[0]
        taskDescription = entry[1]
        taskPriority = entry[2]
        tastDueDate = entry[3]
        taskProjectName = entry[4]

        command = arguments[1].lower()
        if len(arguments) > 2:
            parameter = arguments[2].lower()

        if command == '-name':
            if taskName.lower() == parameter:
                for item in entry:
                    print item
                    print '\t'
            print '\n'
        elif command == '-description':
            flag = 1
            if taskDescription.lower() == parameter:
                for item in entry:
                    print item
                    print '\t'
            print '\n'
        elif command == '-priority':
            flag = 1
            if taskPriority.lower() == parameter:
                for item in entry:
                    print item
                    print '\t'
            print '\n'
        elif command == '-duedate':
            flag = 1
            if taskDueDate.lower() == parameter:
                for item in entry:
                    print item
                    print '\t'
            print '\n'
        elif command == '-projectname':
            flag = 1
            if taskProjectName.lower() == parameter:
                for item in entry:
                    print item
                    print '\t'
            print '\n'
        elif command == 'create':
            flag = 1
            create_task()
            break
        elif command == 'search':
            flag = 1
            search(arguments)
            break
        else:
            print '\nIncorrect usage.'
            print '\nUse: create, search, -name -description -priority -duedate -projectname'
            break

    t.close()

arguments = []

for line in sys.argv:
    arguments.append(line)
if len(arguments) > 1:
    redirect(arguments)
else:
    create_task()
